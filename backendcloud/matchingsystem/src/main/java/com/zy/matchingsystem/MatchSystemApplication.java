package com.zy.matchingsystem;

import com.zy.matchingsystem.service.impl.MatchingServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class MatchSystemApplication {
    public static void main(String[] args) {
        MatchingServiceImpl.matchingpool.start();
        SpringApplication.run(MatchSystemApplication.class, args);
    }
}

