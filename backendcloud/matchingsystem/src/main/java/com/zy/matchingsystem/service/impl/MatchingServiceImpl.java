package com.zy.matchingsystem.service.impl;

import com.zy.matchingsystem.service.MatchingService;
import com.zy.matchingsystem.service.impl.utils.MatchingPool;
import org.springframework.stereotype.Service;

@Service
public class MatchingServiceImpl implements MatchingService {

    public static final MatchingPool matchingpool = new MatchingPool();
    @Override
    public String addPlayer(Integer userId, Integer rating) {
        System.out.println("ADD player: " + userId+" "+rating);
        matchingpool.addPlayer(userId,rating);
        return "add player";
    }

    @Override
    public String removePlayer(Integer userId) {
        System.out.println("Remove-Player :" + userId);
        matchingpool.removePlayer(userId);
        return "remove player";
    }
}
