package com.zy.matchingsystem.service.impl.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class MatchingPool extends Thread{

    private static List<Player> players = new ArrayList<>();
    private final ReentrantLock lock = new ReentrantLock();
    private static RestTemplate restTemplate;
    private static final String startGameUrl = "http://127.0.0.1:3000/pk/start/game/";
    @Autowired
    private void setRestTemplate(RestTemplate restTemplate){
        MatchingPool.restTemplate = restTemplate;
    }
    public void addPlayer(Integer userId,Integer rating){
        // 在多个线程(匹配线程遍历players时，主线程调用方法时)会操作players变量，因此加锁
        lock.lock();
        try {
            players.add(new Player(userId,rating,0));
        }finally {
            lock.unlock();
        }
    }
    public void removePlayer(Integer userId){
        lock.unlock();
        try{
            List<Player> newPlayers = new ArrayList<>();
            for(Player player : players){
                if(!player.getUserId().equals(userId)){
                    newPlayers.add(player);
                }
            }
            players = newPlayers;
        }finally {
            lock.unlock();
        }
    }
    private void increaseWaitingTime(){
        for(Player player : players){
            player.setWaitingtime(player.getWaitingtime()+1);
        }
    }
    private boolean checkMatched(Player a,Player b){
        int ratingDelte = Math.abs(a.getRating() - b.getRating());
        int waitingTime = Math.min(a.getWaitingtime(), b.getWaitingtime());
        return ratingDelte<=waitingTime*10;
    }
    private void sendResult(Player a,Player b){
        System.out.println("send Result:" +a +" "+ b);
        MultiValueMap<String,String>data = new LinkedMultiValueMap<>();
        data.add("a_id",a.getUserId().toString());
        data.add("b_id",b.getUserId().toString());
        restTemplate.postForObject(startGameUrl,data,String.class);

    }
    private void matchPlayers(){
        System.out.println("match players:"+players.toString());
        boolean[] used = new boolean[players.size()];
        for (int i = 0; i < players.size(); i++) {
            if(used[i])continue;
            for (int j = i+1; j < players.size(); j++) {
                if(used[j])continue;
                Player a = players.get(i),b = players.get(j);
                if(checkMatched(a,b)){
                    used[i] = used[j]=true;
                    sendResult(a,b);
                    break;
                }

            }
        }
        List<Player>newPlayers = new ArrayList<>();
        for (int i = 0; i < players.size(); i++) {
            if(!used[i]){
                newPlayers.add(players.get(i));
            }
        }
        players = newPlayers;
    }
    @Override
    public void run() {
        while (true){
            try{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                lock.lock();
                increaseWaitingTime();
                matchPlayers();
            }
            finally {
                lock.unlock();
            }
        }
    }
}
