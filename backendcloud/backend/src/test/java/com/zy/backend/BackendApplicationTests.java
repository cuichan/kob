package com.zy.backend;

import com.zy.backend.mapper.RecordMapper;
import com.zy.backend.pojo.Record;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;

@SpringBootTest
class BackendApplicationTests {

    @Autowired
    private RecordMapper recordMapper;
    @Test
    void contextLoads() {
       Record record = new Record(null,1,1,1,1,1,1,"a","b","a","a",new Date());
       recordMapper.insert(record);
    }

}
