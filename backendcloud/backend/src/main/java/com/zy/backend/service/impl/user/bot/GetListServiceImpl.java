package com.zy.backend.service.impl.user.bot;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.backend.mapper.BotMapper;
import com.zy.backend.pojo.Bot;
import com.zy.backend.pojo.User;
import com.zy.backend.service.user.bot.GetListService;
import com.zy.backend.utils.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetListServiceImpl implements GetListService {

    @Autowired
    private BotMapper botMapper;


    @Override
    public List<Bot> getList() {
        User user= UserUtil.getUserContext();
        QueryWrapper<Bot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", user.getId());
        return botMapper.selectList(queryWrapper);
    }
}
