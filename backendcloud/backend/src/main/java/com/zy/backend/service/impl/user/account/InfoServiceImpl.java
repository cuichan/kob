package com.zy.backend.service.impl.user.account;

import com.zy.backend.pojo.User;
import com.zy.backend.service.user.account.InfoService;
import com.zy.backend.utils.UserUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class InfoServiceImpl implements InfoService {


    @Override
    public Map<String, String> getInfo() {
        User user = UserUtil.getUserContext(); //获取登录的用户;
        Map<String,String> map = new HashMap<>();
        map.put("error_message","success");
        map.put("id",user.getId().toString());
        map.put("username",user.getUsername());
        map.put("photo", user.getPhoto());

        return map;
    }
}
