package com.zy.backend.consumer;


import javax.websocket.*;
import javax.websocket.server.PathParam;

import com.alibaba.fastjson2.JSONObject;
import com.zy.backend.consumer.util.Game;
import com.zy.backend.consumer.util.JwtAuthentication;
import com.zy.backend.mapper.RecordMapper;
import com.zy.backend.mapper.UserMapper;
import com.zy.backend.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;


@Component
@ServerEndpoint("/websocket/{token}")  // 注意不要以'/'结尾
public class WebSocketServer {


    //此处每一个变量在每一个线程里面，所以需要是线程安全的;
    public static final ConcurrentHashMap<Integer,WebSocketServer> users = new ConcurrentHashMap<>();


    private User user;
    private Game game = null;
    private Session session = null;
    private static UserMapper userMapper;
    public static RecordMapper recordMapper;
    private static RestTemplate restTemplate;
    private final static String addPlayerUrl = "http://127.0.0.1:3001/player/add/";
    private final static String removePlayerUrl = "http://127.0.0.1:3001/player/remove/";
    @Autowired
    public void setUserMapper(UserMapper userMapper){
        WebSocketServer.userMapper = userMapper;
    }
    @Autowired
    public void setRecordMapper(RecordMapper recordMapper){
        WebSocketServer.recordMapper = recordMapper;
    }
    @Autowired
    private void setRestTemplate(RestTemplate restTemplate){
        WebSocketServer.restTemplate = restTemplate;
    }
    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) throws IOException {
        // 建立连接
        this.session = session;
        System.out.println("Connect!!!");
        Integer userId = JwtAuthentication.getUserId(token);
        this.user = userMapper.selectById(userId);
        if(this.user != null){
            users.put(userId,this);
//            System.out.println(user);
        }else {
            this.session.close();;
        }

    }

    @OnClose
    public void onClose() {
        // 关闭链接
        System.out.println("Disconnect!!!");
        if(this.user !=null){
            users.remove(this.user.getId());
        }
    }

    public static void startGame(Integer aId,Integer bId){
        User a = userMapper.selectById(aId),b = userMapper.selectById(bId);
        System.out.println("A是==>"+a);
        System.out.println("B是++>"+b);
        //局部变量，创建地图；
        Game game = new Game(13,14,20,a.getId(),b.getId());

        // 一局游戏一个线程，会执行game类的run方法
        game.start();
        if(users.get(a.getId())!=null)users.get(a.getId()).game = game;
        if(users.get(b.getId())!=null)users.get(b.getId()).game = game;
        JSONObject respGame = new JSONObject();
        // 玩家的id以及横纵信息
        respGame.put("a_id", game.getPlayerA().getId());
        respGame.put("a_sx", game.getPlayerA().getSx());
        respGame.put("a_sy", game.getPlayerA().getSy());
        respGame.put("b_id", game.getPlayerB().getId());
        respGame.put("b_sx", game.getPlayerB().getSx());
        respGame.put("b_sy", game.getPlayerB().getSy());
        respGame.put("map", game.getG());

        JSONObject respA = new JSONObject();
        respA.put("event", "start-matching");
        respA.put("opponent_username", b.getUsername());
        respA.put("opponent_photo", b.getPhoto());
        respA.put("game", respGame);
        //用users获取a的链接
        if(users.get(a.getId())!=null)
            users.get(a.getId()).sendMessage(respA.toJSONString()); //用这个链接将信息传回给前端

        JSONObject respB = new JSONObject();
        respB.put("event", "start-matching");
        respB.put("opponent_username", a.getUsername());
        respB.put("opponent_photo", a.getPhoto());
        respB.put("game", respGame);
        if(users.get(b.getId())!=null)
        //用users获取b的链接
            users.get(b.getId()).sendMessage(respB.toJSONString()); //用这个链接将信息传回给前端
    }
    private void startMatching(){
        System.out.println("开始匹配");
        MultiValueMap<String,String> data = new LinkedMultiValueMap<>();
        data.add("user_id",this.user.getId().toString());
        data.add("rating",this.user.getRating().toString());
        restTemplate.postForObject(addPlayerUrl,data,String.class);

    }
    private void stopMatching(){
        System.out.println("停止匹配");
        MultiValueMap<String,String> data = new LinkedMultiValueMap<>();
        data.add("user_id",this.user.getId().toString());
        restTemplate.postForObject(removePlayerUrl,data,String.class);
    }
    private void move(int direction){

        //取出当前WebSocket实例中的Use进行判断;
        if(game.getPlayerA().getId().equals(user.getId())) {
            game.setNextStepA(direction);
        } else if(game.getPlayerB().getId().equals(user.getId())) {
            game.setNextStepB(direction);
        }
    }
    @OnMessage
    public void onMessage(String message, Session session) {
        // 从Client接收消息
//        System.out.println("ReceiveMessage!!!");
        JSONObject data = JSONObject.parseObject(message);
        String event = data.getString("event");
        if("start-matching".equals(event)){
            startMatching();
        }else if("stop-matching".equals(event)){
            stopMatching();
        }else if("move".equals(event)){
//            System.out.println("读取线程名字是==》"+Thread.currentThread().getName());
            move(data.getInteger("direction"));
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

    public void sendMessage(String message){
        //因为是异步通信，所以需要加锁
        synchronized (this.session){
            try{
                //此API可以向当前这个连接前端发送信息
               this.session.getBasicRemote().sendText(message);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
