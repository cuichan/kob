package com.zy.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    //当我们需要用到一个东西时，需要加一个Configuration和注解Bean并返回他的一个实例;
    //之后用它的时候使用Autowired就可以自动把他注入进来

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
