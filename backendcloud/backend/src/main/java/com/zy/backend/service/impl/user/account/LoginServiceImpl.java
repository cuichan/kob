package com.zy.backend.service.impl.user.account;

import com.zy.backend.pojo.User;
import com.zy.backend.service.impl.utils.UserDetailsImpl;
import com.zy.backend.service.user.account.LoginService;
import com.zy.backend.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Service
public class LoginServiceImpl implements LoginService {


//    验证登录进来
    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public Map<String, String> getToken(String username, String password) {

        //会把密码和用户名封装成这样一个类，密码在数据库中不是明文;
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        //验证是否正确;

        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        //把这个用户取出来;
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticate.getPrincipal();
        User user = loginUser.getUser();
        ////生成JWT根据ID;
        String jwt = JwtUtil.createJWT(user.getId().toString());
        //存起来之后传给用户;
        Map<String,String> map = new HashMap<>();
        map.put("error_message","success");
        map.put("token",jwt);
        return map;
    }
}
