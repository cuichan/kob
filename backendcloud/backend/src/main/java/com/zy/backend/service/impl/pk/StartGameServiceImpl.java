package com.zy.backend.service.impl.pk;

import com.zy.backend.consumer.WebSocketServer;
import com.zy.backend.service.pk.StartGameService;
import org.springframework.stereotype.Service;

@Service
public class StartGameServiceImpl implements StartGameService {
    @Override
    public String startGame(Integer aId, Integer bId) {
        System.out.println("startGame: "+aId+"和"+bId);
        WebSocketServer.startGame(aId,bId);
        return "start game success!!!";
    }
}
