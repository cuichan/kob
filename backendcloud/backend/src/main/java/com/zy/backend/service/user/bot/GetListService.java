package com.zy.backend.service.user.bot;

import com.zy.backend.pojo.Bot;

import java.util.List;

public interface GetListService {
    List<Bot> getList();
}
