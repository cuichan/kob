import $ from "jquery";

export default {
  state: {
    id: "",
    username: "",
    photo: "",
    token: "",
    is_login: "",
    is_login2:"",
    is_login3:"",
  },
  getters: {},
  mutations: {
    updateUser(state, user) {
      state.id = user.id;
      state.username = user.username;
      state.photo = user.photo;
      state.is_login = user.is_login;
    },
    updateToken(state, token) {
      state.token = token;
    },
    logout(state){
      state.id = "",
      state.username = "",
      state.photo = "",
      state.token = "",
      state.is_login = false;
      state.is_login2 = false;
      state.is_login3 = false;
    }
  },
  actions: {
    login(context, data) {
      $.ajax({
        url: "http://localhost:3000/user/account/token/",
        type: "POST",
        data: {
          username: data.username,
          password: data.password,
        },
        success(resp) {
          // console.log("我抛到了user.js=>success!");
          //在actions里面调用mutations里面的函数需要commit("xxx")
          if (resp.error_message === "success") {
            context.state.is_login2=true;
            context.state.is_login3=true;
            localStorage.setItem("jwt_token",resp.token);
            context.commit("updateToken", resp.token);
            // console.log(context.state.token);
            data.success(resp);
          } else {
            data.error(resp);
          }
        },
        error(resp) {
          // console.log("我抛到了user.js=>error!");
          data.error(resp);
        },
      });
    },
    getinfo(context,data){
      $.ajax({
        url: "http://localhost:3000/user/account/info/",
        type: "GET",
        headers: {
          Authorization:
            "Bearer " +
            context.state.token,
        },
        success(resp) {
          // console.log(resp);
          if(resp.error_message === "success"){
            context.commit("updateUser",{
            ...resp,
            is_login:true,
          });
          data.success(resp);
          }else{
            data.error(resp);
          }
          
        },
        error(resp) {
          data.error(resp);
        },
      });
    },
    logout(context){
      localStorage.removeItem("jwt_token");
      context.commit("logout");
    }
  },
  modules: {},
};
