import { createRouter, createWebHistory } from "vue-router";
import PKIndexView from "../views/pk/PKIndexView";
import RecordIndexView from "../views/record/RecordIndexView";
import RecordContentView from "../views/record/RecordContentView";
import RanklistIndexView from "../views/ranklist/RankListIndexView";
import UserBotIndexView from "../views/user/bot/UserBotIndexView";
import NotFound from "../views/error/NotFoundView";
import HomeView from "../views/HomeView";
import UserAccountLoginView from "../views/user/account/UserAccountLoginView";
import UserAccountRegisterView from "../views/user/account/UserAccountRegisterView";
import SnakePKView from "@/views/pk/SnakePKView";
import UserIndexView from "@/views/user/UserIndexView";
import store from "../store/index";

const routes = [
  {
    path: "/",
    name: "home", //展示主页
    component: HomeView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/user/index/", //用户中心
    name: "user_index",
    component: UserIndexView,
    meta: {
      requestAuth: true,
    },
  },
  {
    path: "/pk/", //游戏展示页面
    component: PKIndexView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/pk/snake/", //游玩界面
    name: "snake_index",
    component: SnakePKView,
    meta: {
      requestAuth: true,
    },
  },
  {
    path: "/record/", //对战记录页面
    name: "record_index",
    component: RecordIndexView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/record/:recordId/", //对局回放
    name: "record_content",
    component: RecordContentView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/ranklist/", //排行榜
    name: "ranklist_index",
    component: RanklistIndexView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/user/bot/",
    name: "user_bot_index",
    component: UserBotIndexView,
    meta: {
      requestAuth: true,
    },
  },
  {
    path: "/user/account/login/",
    name: "user_account_login",
    component: UserAccountLoginView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/user/account/register/",
    name: "user_account_register",
    component: UserAccountRegisterView,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/404/",
    name: "404",
    component: NotFound,
    meta: {
      requestAuth: false,
    },
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/404/",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
router.beforeEach((to, from, next) => {
  let flag = 1;
  const jwt_token = localStorage.getItem("jwt_token");
  if (jwt_token) {
    store.commit("updateToken", jwt_token);
    store.dispatch("getinfo", {
      success() {
        store.state.user.is_login2 = true;
        store.state.user.is_login3 = true;
      },
      error() {
        localStorage.removeItem("jwt_token");
        store.commit("logout");
        router.push({ name: "user_account_login" });
      },
    });
  } else {
    flag = 2;
  }
  if (to.meta.requestAuth && !store.state.user.is_login) {
    if (flag === 1) {
      next();
    } else {
      alert("请先进行登录!");
      next({ name: "user_account_login" });
    }
  } else {
    next();
  }
});
export default router;
